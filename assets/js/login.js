const email = document.getElementById("email")
const password = document.getElementById("pass")
const wrongPass = document.getElementById("wrongpassword")
const notExist = document.getElementById("user-not-exist")
const warning = document.createElement("p")

wrongPass.setAttribute("class", "wrongpassword")

const form = document.getElementById("form")

form.addEventListener("submit", function (e) {
    e.preventDefault()

    const user = localStorage.getItem(email.value)

    if (user !== null) {
        const userData = JSON.parse(user)

        if (password.value === userData.password.toString()) {
            localStorage.setItem("isLoggedIn", 1)
            window.location.href = "/index.html"
        } else {
            warning.innerText = "wrong password"
            wrongPass.appendChild(warning)
        }
    } else {
        notExist.classList.add("active")
    }
})

password.addEventListener("keydown", function () {
    if (this.value !== "") {
        wrongPass.removeChild(warning)
    }
})

email.addEventListener("keydown", function () {
    if (this.value !== "") {
        notExist.classList.remove("active")
    }
})