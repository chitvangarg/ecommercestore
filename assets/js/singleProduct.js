const container = document.getElementById("container")
let loader = document.createElement("div")
let user = document.getElementById("user")

loader.setAttribute("class", "loader")

let errorDiv = document.createElement("div")
errorDiv.setAttribute("class", "error")

async function fetchSingle() {
    try {
        let itemId = localStorage.getItem("id")

        container.appendChild(loader)

        let product = await fetch(`https://fakestoreapi.com/products/${itemId}`)

        container.removeChild(loader)

        if (product !== undefined && JSON.stringify(product).length !== 0) {
            product = await product.json()

            showItem(product)
        } else {
            container.removeChild(loader)
            errorDiv.innerHTML = "No Products"
            container.appendChild(errorDiv)
        }
    } catch (err) {
        container.removeChild(loader)
        errorDiv.innerHTML = "Unable to Fetch data"
        container.appendChild(errorDiv)
    }
}

function showItem(item) {
    let card = document.createElement("article")
    let heading = document.createElement("h3")
    let image = document.createElement("div")
    let img = document.createElement("img")
    let pricing = document.createElement("div")
    let price = document.createElement("p")
    let rating = document.createElement("p")
    let description = document.createElement("div")

    card.setAttribute("class", "card")
    image.setAttribute("class", "product_image")
    pricing.setAttribute("class", "price_section")
    description.setAttribute("class", "description")

    heading.innerHTML = item.title.toString()
    img.setAttribute("src", item.image.toString())
    price.innerHTML = `Price: $${item.price.toString()}`
    rating.innerHTML = `Rating: ${item.rating.rate.toString()}`
    description.innerHTML = item.description

    image.appendChild(img)
    pricing.appendChild(price)
    pricing.appendChild(rating)
    card.appendChild(heading)
    card.appendChild(image)
    card.appendChild(pricing)
    card.appendChild(description)

    container.appendChild(card)
}

//authentication

if (localStorage.getItem("isLoggedIn") == 1) {
    fetchSingle()
} else {
    window.location.href = "login.html"
}

// Logout

user.addEventListener("click", function () {
    localStorage.removeItem("isLoggedIn")
    window.location.reload()
})
