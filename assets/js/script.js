let container = document.getElementById("products_container")
let loader = document.createElement("div")
let errorBox = document.createElement("div")
let user = document.getElementById("user")

errorBox.setAttribute("class", "error")
loader.setAttribute("class", "loader")

async function getData() {
    try {
        container.appendChild(loader)

        let items = await fetch("https://fakestoreapi.com/products")

        container.removeChild(loader)

        items = await items.json()

        if (items !== undefined && items.length !== 0) {
            const products = items.reduce((accum, curr) => {
                if (accum[curr.category] === undefined) {
                    const item = []
                    item.push(curr)
                    accum[curr.category] = item
                } else {
                    accum[curr.category].push(curr)
                }

                return accum
            }, {})

            for (let category in products) {
                addProducts(products[category], category)
            }
        } else {
            container.removeChild(loader)
            errorBox.innerText = "No such product"
            container.appendChild(errorBox)
        }
    } catch (err) {
        container.removeChild(loader)
        errorBox.innerText = "Unable to Fetch data"
        container.appendChild(errorBox)
    }
}

// Function to addproduct card in the specific category
function addProducts(items, category) {
    const categoryContainer = document.createElement("div")
    categoryContainer.setAttribute("class", "categoryContainer")

    const categoryHeading = document.createElement("h1")
    categoryHeading.innerText = category.toString()

    for (let product = 0; product < items.length; product++) {
        let card = document.createElement("article")
        let heading = document.createElement("h3")
        let image = document.createElement("div")
        let img = document.createElement("img")
        let pricing = document.createElement("div")
        let price = document.createElement("p")
        let rating = document.createElement("p")

        card.setAttribute("class", "card")
        image.setAttribute("class", "product_image")
        pricing.setAttribute("class", "price_section")

        heading.innerText = items[product].title.toString()
        img.setAttribute("src", items[product].image.toString())
        price.innerText = `Price: $${items[product].price.toString()}`
        rating.innerText = `Rating: ${items[product].rating.rate.toString()}`

        image.appendChild(img)
        pricing.appendChild(price)
        pricing.appendChild(rating)
        card.appendChild(heading)
        card.appendChild(image)
        card.appendChild(pricing)

        categoryContainer.appendChild(card)
        container.appendChild(categoryHeading)
        container.appendChild(categoryContainer)

        card.addEventListener("click", function () {
            localStorage.setItem("id", items[product].id)
            window.location.href = "/viewProduct.html"
        })
    }
}

if (localStorage.getItem("isLoggedIn") == 1) {
    getData()
} else {
    window.location.href = "login.html"
}

// logout

user.addEventListener("click", function () {
    localStorage.removeItem("isLoggedIn")
    window.location.reload()
})
