const username = document.getElementById("name")
const email = document.getElementById("email")
const password = document.getElementById("pass")
const wrongName = document.getElementById("wrongname")
const emailExist = document.getElementById("emailExist")
const warning = document.createElement("p")

wrongName.setAttribute("class", "wrongname")
emailExist.setAttribute("class", "emailExist")

const form = document.getElementById("form")

function submitForm() {
    const user = {
        name: username.value,
        email: email.value,
        password: password.value,
    }

    const value = JSON.stringify(user)

    localStorage.setItem(email.value.toString(), value)

    window.location.href = "/login.html"
}

function validateUsername(cb) {

    form.addEventListener("submit", function (e) {
        e.preventDefault()
        let user_reg = /^[A-za-z]+$/

        if (
            username.value.match(user_reg) &&
            localStorage.getItem(email.value) === null
        ) {
            cb()
        } else if (
            username.value !== "" &&
            username.value.match(user_reg) === null
        ) {
            warning.innerText = "Username must contain only alphabets"
            wrongName.appendChild(warning)
            username.addEventListener("change", function () {
                if (username.value !== "") {
                    wrongName.removeChild(warning)
                }
            })
        } else if (
            email.value !== "" &&
            localStorage.getItem(email.value) !== null
        ) {
            warning.innerText = "Email already exist"
            emailExist.appendChild(warning)

            email.addEventListener("change", function () {
                if (email.value !== "") {
                    emailExist.removeChild(warning)
                }
            })
        }
    })
}

username.addEventListener("keydown", function(){
    if(this.value === ""){
        wrongName.removeChild(warning)
        wrongName.style.display = "none"
    }
})

email.addEventListener("keydown", function(){
    if(this.value === ""){
        emailExist.removeChild(warning)
    }
})



validateUsername(submitForm)
